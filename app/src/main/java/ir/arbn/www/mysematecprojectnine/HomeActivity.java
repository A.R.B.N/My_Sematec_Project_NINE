package ir.arbn.www.mysematecprojectnine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ir.arbn.www.mysematecprojectnine.models.StudentModel;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username, password, mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindViews();
    }

    private void bindViews() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            String usernameVal = username.getText().toString();
            String passwordVal = password.getText().toString();
            String mobileVal = mobile.getText().toString();
            StudentModel std = new StudentModel();
            std.setUsername(usernameVal);
            std.setPassword(passwordVal);
            std.setMobile(mobileVal);
            std.setMarried(true);
            std.save();
            username.setText("");
            password.setText("");
            mobile.setText("");
            Toast.makeText(this, "New User Has Been Saved...", Toast.LENGTH_SHORT).show();
        }
    }
}
