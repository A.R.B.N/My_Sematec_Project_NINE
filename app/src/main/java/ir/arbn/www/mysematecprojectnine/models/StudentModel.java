package ir.arbn.www.mysematecprojectnine.models;

import com.orm.SugarRecord;

/**
 * Created by A.R.B.N on 2/13/2018.
 */

public class StudentModel extends SugarRecord<StudentModel> {
    String username;
    String password;
    String mobile;
    Boolean married;

    public StudentModel() {
    }

    public StudentModel(String username, String password, String mobile, Boolean married) {
        this.username = username;
        this.password = password;
        this.mobile = mobile;
        this.married = married;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getMarried() {
        return married;
    }

    public void setMarried(Boolean married) {
        this.married = married;
    }
}
