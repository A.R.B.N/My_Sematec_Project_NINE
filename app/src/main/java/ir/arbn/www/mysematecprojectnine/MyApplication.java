package ir.arbn.www.mysematecprojectnine;

import android.app.Application;

import com.orm.SugarApp;

/**
 * Created by A.R.B.N on 2/13/2018.
 */

public class MyApplication extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
